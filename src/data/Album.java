package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Album implements Serializable{

	transient private SimpleStringProperty name;
    transient private SimpleStringProperty date;
    transient private SimpleStringProperty range;
    transient private SimpleStringProperty num;
    
    private String dataName, dataDate, dataRange, dataNum;
    
	public ArrayList<Photo> photos;
	
	public Album(String name) {
		photos = new ArrayList<Photo>();
		this.dataName = name;
		this.name = new SimpleStringProperty(name);
	}
	
	/**
	 *Sets nameProperty of the album for use in TableView
	 *
	 * @author Elby (egb37)
	 * @param name	Name of the album
	 */
	public void setNameProperty(String name){
		this.name.set(name);
		this.dataName = name;
	}
	
	/**
	 * Gets the name of the album as a string
	 *
	 * @author Elby (egb37)
	 * @return String name of the album
	 */
	public String getName(){
		return this.name.get();
	}
	
	/**
	 * Name of the album as a StringProperty
	 * For use by the TableView
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of the name of the album
	 */
	public StringProperty nameProperty() {
	    return name;
	}

	/**
	 * Date here references the earliest date in the album. If no photos are in the album, set property to "No Data"
	 * For use by the TableView for displaying earliest date
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of the earliest date in the album
	 */
	public StringProperty dateProperty() {
		Date temp;
		if(photos.size() > 0)
			temp = photos.get(0).dateTaken;
		else
			return new SimpleStringProperty("No Data");
		for(Photo photo : photos){
			if(temp.compareTo(photo.dateTaken) > 0)
				temp = photo.dateTaken;
		}
		dataDate = temp.toGMTString();
		return new SimpleStringProperty(temp.toGMTString());
	}
	
	/**
	 * Range of dates (earliest to latest) as a StringProperty
	 * For use by the TableView
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of the range of dates of photos in the album
	 */
	public StringProperty rangeProperty() {
		Date temp;
		
		if(photos.size() > 0)
			temp = photos.get(0).dateTaken;
		else
			return new SimpleStringProperty("No Data");
		
		
		for(Photo photo : photos){
			if(temp.compareTo(photo.dateTaken) < 0)
				temp = photo.dateTaken;
		}
		this.dataRange = dateProperty().get() + " - " + temp.toGMTString();
		return new SimpleStringProperty(dateProperty().get() + " - " + temp.toGMTString());
	}
	
	/**
	 * Initializes the StringProperties of the album.
	 * This is necessary because the objects are transient and cannot be serialized, 
	 * so these properties are set upon reading the objects
	 *
	 * @author Alex (aer112)
	 */
	public void albumInit(){
		name = new SimpleStringProperty(dataName);
		date = new SimpleStringProperty(dataDate);
		range = new SimpleStringProperty(dataRange);
		num = new SimpleStringProperty(dataNum);
	}
	
	/**
	 * Gets the StringProperty for the number of photos in the album
	 *
	 * @author Elby (egb37)
	 * @return StringProperty number of photos in the album
	 */
	public StringProperty numProperty() {
		dataNum = ""+photos.size();
		return new SimpleStringProperty("" + photos.size());
	}
	
	@Override
	public String toString(){
		return this.getName();
	}

}
