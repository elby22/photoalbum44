package data;

import java.util.Date;

import javafx.collections.ObservableList;
/**
 * Encapsulates the information necessary to query a user's library and search for photos
 *
 * @author Elby (egb37)
 * @author Alex (aer112)
 */
public class SearchQuery {
	
	private Date beginningDate;
	private Date endDate;
	private ObservableList<Tag> tags;

	/**
	 * Instantiates SearchQuery with a list of tags, and date range
	 * 
	 * @author Elby (egb37)
	 * @param tags a list of tags to filter photos by
	 * @param beginningDate lowerbound of the date range to search on
	 * @param endDate upperbound of the date range to search on
	 */
	public SearchQuery(ObservableList<Tag> tags, Date beginningDate, Date endDate) {
		this.beginningDate = beginningDate;
		this.endDate = endDate;
		this.tags = tags;
	}
	/**
	 * Returns an album with photos from parameter meeting the search criteria in this instance.
	 *
	 * @author Elby (egb37)
	 * @param album to be searched on (usually a compilation of all photos in library)
	 * @param name of the album
	 */
	public Album executeSearch(Album album, String name){
		Album returnAlbum = new Album(name);
	
		if(beginningDate != null)
			for(Photo photo : album.photos){
				if(photo.dateTaken.compareTo(beginningDate) >= 0) returnAlbum.photos.add(photo);
			}
		
		if(endDate != null)
			for(Photo photo : album.photos){
				if(photo.dateTaken.compareTo(endDate) <= 0) returnAlbum.photos.add(photo);
			}
		
		if(!tags.isEmpty()){
			for(Tag searchTag : tags){
				String searchType = searchTag.typeProperty().get();
				String searchValue = searchTag.valueProperty().get();
				for(Photo photo : album.photos){
					for(Tag tag : photo.tags){
						if(searchType.equals(tag.typeProperty().get()) && searchValue.equals(tag.valueProperty().get()))
							returnAlbum.photos.add(photo);
					}
				}
			}
		}
		
		return returnAlbum;
	}

}
