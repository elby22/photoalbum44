package data;

import java.io.Serializable;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Represents a tag, which is a pair of strings to help with searching and categorization
 *
 * @author Elby (egb37)
 * @author Alex (aer112)
 */
public class Tag implements Serializable{
	
    transient private SimpleStringProperty type;
    transient private SimpleStringProperty value;
    
    private String dataType;
    private String dataValue;
    
	/**
	 * Instantiates a Tag object
	 * 
	 * @author Elby (egb37)
	 * @param type of tag
	 * @param value of tag
	 */
    public Tag(String type, String value){
    	dataType = type;
    	dataValue = value;
    	this.type = new SimpleStringProperty(type);
    	this.value = new SimpleStringProperty(value);
    }
    
	/**
	 * Initializes the StringProperties of the Tag.
	 * This is necessary because the objects are transient and cannot be serialized, 
	 * so these properties are set upon reading the objects
	 *
	 * @author Alex (aer112)
	 */
    public void tagInit(){
    	type = new SimpleStringProperty(dataType);
    	value = new SimpleStringProperty(dataValue);
    }
    
	/**
	 * tag type property
	 * For use by the TableView for displaying type
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of type
	 */
	public StringProperty typeProperty() {
	    return type;
	}
	
	/**
	 * tag value property
	 * For use by the TableView for displaying value
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of value
	 */
	public StringProperty valueProperty() {
	    return value;
	}
	
}