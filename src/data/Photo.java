package data;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
/**
 * This class represents a photo object 
 *
 * @author Elby (egb37)
 * @author Alex (aer112)
 */
public class Photo implements Serializable{

	public Date dateTaken;
	
    transient private SimpleStringProperty caption;
    transient private SimpleStringProperty path;
	transient private SimpleObjectProperty<Image> pImage;
    
	private String dataCaption;
	private String dataPath;
	
	public ArrayList<Tag> tags;
	transient public Image image;
	
	/**
	 * Instantiates the Properties of the Photos because the properties themselves cannot be serialized
	 *
	 * @author Alex (aer112)
	 */
	public void photoInit(){
		caption = new SimpleStringProperty(dataCaption);
		path = new SimpleStringProperty(dataPath);
		
		image = new Image("file:///" + dataPath, true);
		pImage = new SimpleObjectProperty<Image>(this.image);
		
	}
	
	/**
	 * Date the photo was taken
	 * For use by the TableView for displaying date
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of the date of the album
	 */
	public StringProperty dateProperty() {
	    return new SimpleStringProperty(dateTaken.toGMTString());
	}
	
	/**
	 * Caption of the photo
	 * For use by the TableView for displaying caption
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of caption
	 */
	public StringProperty captionProperty() {
	    return caption;
	}
	
	/**
	 * Absolute path of the photo on the filesystem
	 * For use by the TableView for displaying path
	 *
	 * @author Elby (egb37)
	 * @return StringProperty of path
	 */
	public StringProperty pathProperty() {
	    return path;
	}
	
	/**
	 * Sets the caption of the photo
	 *
	 * @author Elby (egb37)
	 * @param String of the the value of the caption
	 */
	public void setCaptionProperty(String caption){
		dataCaption = caption;
		this.caption.set(caption);
		
	}
	
	public SimpleObjectProperty<Image> pImageProperty(){
		return pImage;	
	}
	
	/**
	 * Instantiates a new photo from a file passed by a filechooser
	 *
	 * @author Alex (aer112)
	 */
	public Photo(File file) {
		this.path = new SimpleStringProperty(file.getAbsolutePath());
		this.tags = new ArrayList<Tag>();
		
		dataPath = file.getAbsolutePath();
		
		this.caption = new SimpleStringProperty("");
		
		this.image = new Image("file:///" + dataPath, true);
		this.pImage = new SimpleObjectProperty<Image>(this.image);
		this.dateTaken = new Date(file.lastModified());

	}
}
