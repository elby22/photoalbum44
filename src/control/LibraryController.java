package control;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

import data.*;


/**
 * Controls the Library.fxml user interface and provides functionality to all FXML objects
 *
 * @author Alex (aer112)
 * @author Elby (egb377)
 */
public class LibraryController implements Initializable{

	@FXML private TableView<Album> albums;
	@FXML private TableColumn<Album, String> nameCol, oldestCol, rangeCol, numCol;
	@FXML private TableView<Tag> searchTagTable;
	@FXML private TableColumn<Tag, String> tagTypeCol, tagValCol;
	@FXML private TextField nameText, tagTypeText, tagValText;
	@FXML private DatePicker beginningDatePicker, endDatePicker;
	@FXML private CheckBox createAlbumCheck;
	@FXML private Label usernameLabel;
	
	@FXML private Button logout;
	
	User user;

	ObservableList<Album> obsAlbums;
	ObservableList<Tag> searchTags = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		nameCol.setCellValueFactory(new PropertyValueFactory<Album, String>("name"));	
		oldestCol.setCellValueFactory(new PropertyValueFactory<Album, String>("date"));
		rangeCol.setCellValueFactory(new PropertyValueFactory<Album, String>("range"));
		numCol.setCellValueFactory(new PropertyValueFactory<Album, String>("num"));		
		
		tagTypeCol.setCellValueFactory(new PropertyValueFactory<Tag, String>("type"));
		tagValCol.setCellValueFactory(new PropertyValueFactory<Tag, String>("value"));

		searchTagTable.setItems(searchTags);
		
		albums.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>(){
			public void changed(ObservableValue<? extends Album> ov, Album old_val, Album new_val) {
				if(new_val == null){
					nameText.setText("");
				}else{
					nameText.setText(new_val.getName());
				}
			}
		});
		

	}
	
	
    //not working?
    
	@FXML private void logout(ActionEvent action){
		System.out.println("logout");
		try{
			Stage stage = new Stage();
			stage.setTitle("Login");
			AnchorPane myPane = (AnchorPane)FXMLLoader.load(getClass().getClassLoader().getResource("view/Login.fxml"));
			Scene scene = new Scene(myPane);
			stage.setScene(scene);
			stage.show();
			
		    Node  source = (Node)  action.getSource(); 
		    Stage stage2  = (Stage) source.getScene().getWindow();
		    stage2.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
    @FXML protected void handleAddButtonAction(ActionEvent event) {
    	if(nameText.getText().equals(""))
    		return;
    	
    	for(Album album : user.albums){
    		if(album.getName().equals(nameText.getText()))
    			return;
    	}
    	
    	Album album = new Album(nameText.getText());
    	user.albums.add(album);
    	obsAlbums.add(album);
    	User.serialize();
    }
    
    @FXML protected void handleDeleteButtonAction(ActionEvent event) {
		Album album = albums.getSelectionModel().getSelectedItem();
		user.albums.remove(album);
    	obsAlbums.remove(album);
    	User.serialize();

    }
	
    @FXML protected void handleRenameButtonAction(ActionEvent event) {
    	
    	for(Album album : user.albums){
    		if(album.getName().equals(nameText.getText()))
    			return;
    	}
    	
		Album album = albums.getSelectionModel().getSelectedItem();
    	album.setNameProperty(nameText.getText());
    	user.albums.remove(album);
    	user.albums.add(album);
    	User.serialize();

    }
	
    //Handles tags to be searched with
    @FXML protected void handleAddTagButtonAction(ActionEvent event) {
    	if(tagTypeText.getText().equals("") || tagValText.getText().equals(""))
    		return;
    	for(Tag tag : searchTags){
    		if(tag.typeProperty().get().equals(tagTypeText.getText()) && tag.valueProperty().get().equals(tagValText.getText()))
    			return;
    	}
    	Tag tag = new Tag(tagTypeText.getText(), tagValText.getText());
    	searchTags.add(tag);
    }
    
    @FXML protected void handleRemoveTagButtonAction(ActionEvent event) {
		Tag tag = searchTagTable.getSelectionModel().getSelectedItem();
    	searchTags.remove(tag);
    }
	
    //Grabs all info from fields as well as all tags, then moves to search function
    @FXML protected void handleSearchButtonAction(ActionEvent event) throws IOException {
		Album album = new Album("Empty Search");
    	for(Album a : user.albums){
    		for(Photo p : a.photos){
    			if(!album.photos.contains(p)){
    				album.photos.add(p);
    			}
    		}
    	}
    	
    	LocalDate localDate = beginningDatePicker.getValue();
    	Instant instant;
    	Date beginningDate = null;
    	Date endDate = null;
    	
    	if(localDate != null){
    		instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
    		beginningDate = Date.from(instant);
    	}
    	
    	localDate = endDatePicker.getValue();   	
    	if(localDate != null){
    		instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
    		endDate = Date.from(instant);
    	}
    	
		SearchQuery query = new SearchQuery(searchTags, beginningDate, endDate);
        FXMLLoader searchLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/Album.fxml"));
        Pane searchPane = (Pane)searchLoader.load(); 
        AlbumController controller = (AlbumController) searchLoader.getController();
       
        Stage stage = new Stage();
		if(createAlbumCheck.isSelected()){
	        stage.setTitle("New Album From Search Results");
	        Album temp = query.executeSearch(album, "New Album From Search Results");
	        controller.setUser(user, temp);
	        user.albums.add(temp);
	        obsAlbums.add(temp);
		}else{
	        stage.setTitle("Search Results");
	        controller.setUser(user, query.executeSearch(album, "Search Results"));
		}
		
        Scene scene = new Scene(searchPane);
        stage.setScene(scene);
        stage.show();
        
        stage = (Stage) albums.getScene().getWindow();
        stage.close();
    }
    
    @FXML protected void handleOpenButtonAction(ActionEvent event) throws IOException {

        FXMLLoader searchLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/Album.fxml"));
        Pane searchPane = (Pane)searchLoader.load(); 
        AlbumController controller = (AlbumController) searchLoader.getController();
		Album album = albums.getSelectionModel().getSelectedItem();
		
        Stage stage = new Stage();
        stage.setTitle(album.getName());
        
        controller.setUser(user, album);		
        Scene scene = new Scene(searchPane);
        stage.setScene(scene);
        stage.show();
        
        stage = (Stage) albums.getScene().getWindow();
        stage.close();
        
    }
    
    /**
     * Sets the properties of the user before initializing the FXML Interface to display the required data.
     *
     * @author Alex (aer112)
     * @author Elby (egb377)
     * @param user User to be passed to the Library controller, the one who's library is to be displayed
     */
    public void setUser(User user){
    	this.user = user;
    	obsAlbums = FXCollections.observableArrayList(user.albums);
		albums.setItems(obsAlbums);
    	usernameLabel.setText(user.name);
		if(!obsAlbums.isEmpty()) albums.getSelectionModel().select(0);
    }
}
