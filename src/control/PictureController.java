package control;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import data.Album;
import data.Photo;
import data.Tag;
import data.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
/**
 * Controls the Login.fxml user interface and provides functionality to all FXML objects
 *
 * @author Elby (egb37)
 */
public class PictureController implements Initializable{
	
	Album album;
	
	private @FXML ImageView imageView;
	private @FXML TableView tagTable;
	private @FXML TableColumn tagTypeCol, tagValCol;
	private @FXML Label dateLabel, captionLabel;
	
	ObservableList<Tag> tags = FXCollections.observableArrayList();
	int imageIndex = 0;

	public void initialize(URL arg0, ResourceBundle arg1) {
		tagTypeCol.setCellValueFactory(new PropertyValueFactory<Tag, String>("type"));
		tagValCol.setCellValueFactory(new PropertyValueFactory<Tag, String>("value"));
	}
	
    @FXML protected void handlePreviousButtonAction(ActionEvent event) {
    	imageIndex--;
    	if(imageIndex < 0)
    		imageIndex = album.photos.size() - 1;
    	
		setImageData(imageIndex);
    }
    
    @FXML protected void handleNextButtonAction(ActionEvent event) {
    	imageIndex++;
    	if(imageIndex > album.photos.size() - 1)
    		imageIndex = 0;
    	
    	setImageData(imageIndex);
    }
    
    /**
     * Sets the data for the photo to be displayed in the ImageView
     * @author Elby (egb37)
     * @param index in the album.photos list of the photo to be displayed by the ImageView  
     */
    private void setImageData(int index){
    	Photo photo = album.photos.get(imageIndex);
		imageView.setImage(photo.image);
	
		dateLabel.setText(photo.dateProperty().get());
		captionLabel.setText(photo.captionProperty().get());
		
		tags = FXCollections.observableArrayList(album.photos.get(index).tags);
		
		tagTable.setItems(tags);
    }
    /**
     * Sets the data for the album/pictures to be displayed in this screen before initialization
     * @author Elby (egb37)
     * @param album to be used in this screen
     * @param index of the photo to be opened first in the album.photos list
     */
    public void setAlbum(Album album, int index){
    	this.album = album;
		imageIndex = index;
		setImageData(imageIndex);
    }

}
