package control;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import data.Album;
import data.Photo;
import data.Tag;
import data.User;
import javafx.scene.control.Button;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Controls the Album.fxml user interface and provides functionality to all FXML objects
 *
 * @author Alex (aer112)
 * @author Elby (egb377)
 */
public class AlbumController implements Initializable{
	private Album album;
	private User user;

	@FXML private TableView<Photo> pictureTable;
	@FXML private TableColumn<Photo, String> dateCol, filePathCol;
	@FXML private TableColumn<Photo, Image> thumbCol;

	@FXML private TableView<Tag> tagTable;
	@FXML private TableColumn<Tag, String> tagTypeCol, tagValCol;
	@FXML private TextField captionText, tagTypeText, tagValueText;
	@FXML private Label albumLabel, usernameLabel;
	@FXML private ChoiceBox<Album> albumsChoiceBox;

	@FXML private CheckBox copyPhotoCheck;
	@FXML private Button logout;
	

	ObservableList<Photo> obsPhotos;
	ObservableList<Tag> tags = FXCollections.observableArrayList();
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		dateCol.setCellValueFactory(new PropertyValueFactory<Photo, String>("date"));
		filePathCol.setCellValueFactory(new PropertyValueFactory<Photo, String>("path"));
		
		tagTypeCol.setCellValueFactory(new PropertyValueFactory<Tag, String>("type"));
		tagValCol.setCellValueFactory(new PropertyValueFactory<Tag, String>("value"));
		
		thumbCol.setCellValueFactory(new PropertyValueFactory<Photo, Image>("pImage"));
		
		tagTable.setItems(tags);
		thumbCol.setCellFactory(new Callback<TableColumn<Photo, Image>, TableCell<Photo, Image>>(){

			@Override
			public TableCell<Photo, Image> call(TableColumn<Photo, Image> arg0) {
				return new TableCell<Photo, Image>(){
					
					VBox vb = new VBox();
					ImageView iv = new ImageView();
					
					{
						vb.setAlignment(Pos.CENTER);
						iv.setFitHeight(50);
						iv.setFitWidth(50);
						vb.getChildren().addAll(iv);
						setGraphic(vb);
					}
				
					@Override
					public void updateItem(Image image, boolean empty){
				        super.updateItem(image, empty);
						if(image != null){
							iv.setImage(image);
						}else{
							iv.setImage(null);
						}
					}
				};
			}
		});
			
		pictureTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Photo>(){
			public void changed(ObservableValue<? extends Photo> ov, Photo old_val, Photo new_val) {
				if(new_val == null){
					captionText.setText("");
				}else{
					captionText.setText(new_val.captionProperty().get());
					tags = FXCollections.observableArrayList(new_val.tags);
					tagTable.setItems(tags);
				}
			}
		});
		

	}

	@FXML private void logout(ActionEvent action){
		System.out.println("logout");
		try{
			Stage stage = new Stage();
			stage.setTitle("Login");
			AnchorPane myPane = (AnchorPane)FXMLLoader.load(getClass().getClassLoader().getResource("view/Login.fxml"));
			Scene scene = new Scene(myPane);
			stage.setScene(scene);
			stage.show();
			
		    Node  source = (Node)  action.getSource(); 
		    Stage stage2  = (Stage) source.getScene().getWindow();
		    stage2.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
    @FXML protected void handleAddPhotoButtonAction(ActionEvent event) {
    	 Stage stage = new Stage();
    	 FileChooser fileChooser = new FileChooser();
    	 fileChooser.setTitle("Open Resource File");
    	 fileChooser.getExtensionFilters().addAll(
    	         new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
    	 File selectedFile = fileChooser.showOpenDialog(stage);
    	 if (selectedFile != null) {
    		//Check for duplicate files
    		for(Photo p : album.photos){
        		if((selectedFile.getAbsolutePath()).equals(p.pathProperty().get()))
        			return;
    		}
    	    Photo photo = new Photo(selectedFile);
    	    album.photos.add(photo);
    	    obsPhotos.add(photo);
    	    pictureTable.setItems(obsPhotos);
    	    User.serialize();
    	 }
    }
    
    @FXML protected void handleEditCaptionButtonAction(ActionEvent event) {
		Photo photo = pictureTable.getSelectionModel().getSelectedItem();
		if(photo != null)
			photo.setCaptionProperty(captionText.getText());
		
		User.serialize();
    }
    
    @FXML protected void handleRemovePhotoButtonAction(ActionEvent event) {
		Photo photo = pictureTable.getSelectionModel().getSelectedItem();
    	album.photos.remove(photo);
    	obsPhotos.remove(photo);
    	User.serialize();
    }
    
    @FXML protected void handleCloseAlbumButtonAction(ActionEvent event) throws IOException {
        Stage stage = (Stage) pictureTable.getScene().getWindow();
        stage.close();
        
        FXMLLoader libraryLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/Library.fxml"));
        Pane libraryPane = (Pane)libraryLoader.load(); 
        LibraryController controller = (LibraryController) libraryLoader.getController();

        stage = new Stage();
        stage.setTitle("Library");
        Scene scene = new Scene(libraryPane);

        controller.setUser(user);   
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML protected void handlviewPictureButton(ActionEvent event) throws IOException {

        FXMLLoader pictureLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/Picture.fxml"));
        Pane pane = (Pane)pictureLoader.load(); 
        PictureController controller = (PictureController) pictureLoader.getController();
		Photo photo = pictureTable.getSelectionModel().getSelectedItem();
      
        Stage stage = new Stage();
        stage.setTitle("Photos from " + album.getName());

        controller.setAlbum(album, album.photos.indexOf(photo)); 
        
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML protected void handleMovePhotoButtonAction(ActionEvent event) {
		Photo photo = pictureTable.getSelectionModel().getSelectedItem();
		Album album = albumsChoiceBox.getValue();
		for(Photo p : album.photos){
			if(photo.pathProperty().get().equals(p.pathProperty().get()))
				return;
		}
		album.photos.add(photo);
		if(!copyPhotoCheck.isSelected()){
			this.album.photos.remove(photo);
			obsPhotos.remove(photo);
		}
	    pictureTable.setItems(obsPhotos);

    }
    
    @FXML protected void handleAddTagButtonAction(ActionEvent event) {
    	if(tagTypeText.getText().equals("") || tagValueText.getText().equals(""))
    		return;
    	for(Tag tag : tags){
    		if(tag.typeProperty().get().equals(tagTypeText.getText()) && tag.valueProperty().get().equals(tagValueText.getText()))
    			return;
    	}
    	Tag tag = new Tag(tagTypeText.getText(), tagValueText.getText());
		Photo photo = pictureTable.getSelectionModel().getSelectedItem();
		photo.tags.add(tag);
		this.tags.add(tag);
		tagTable.setItems(tags);
		tagTypeText.setText("");
		tagValueText.setText("");
		User.serialize();
    }
    
    @FXML protected void handleRemoveTagButtonAction(ActionEvent event) {
		Tag tag = tagTable.getSelectionModel().getSelectedItem();
		Photo photo = pictureTable.getSelectionModel().getSelectedItem();
    	photo.tags.remove(tag);
    	this.tags.remove(tag);
		tagTable.setItems(tags);
		User.serialize();
    }
    
    /**
     * Sets the properties of the user and album before initializing the FXML Interface to display the required data.
     *
     * @author Alex (aer112)
     * @author Elby (egb377)
     * @param user who is currently logged into the session
     * @param album to be displayed
     */
    public void setUser(User user, Album album){
    	this.album = album;
		this.user = user;
		obsPhotos = FXCollections.observableArrayList(album.photos);
		pictureTable.setItems(obsPhotos);
		albumLabel.setText(album.getName());
		usernameLabel.setText(user.name);
		ObservableList<Album> albums = FXCollections.observableArrayList(user.albums);
		albumsChoiceBox.setItems(albums);
		if(!obsPhotos.isEmpty()) pictureTable.getSelectionModel().select(0);
    }

}
