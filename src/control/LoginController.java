package control;

import java.net.URL;
import java.util.ResourceBundle;

import java.io.*;

import data.Album;
import data.Photo;
import data.Tag;
import data.User;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.Node;


import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.fxml.FXMLLoader;


/**
 * Controls the Login.fxml user interface and provides functionality to all FXML objects
 *
 * @author Alex (aer112)
 */
public class LoginController implements Initializable{
	public boolean newLib;
	public boolean loginChk;
	
	@FXML private Button Login;
	@FXML private TextField  loginText;
		
	@Override
	public void initialize(URL url, ResourceBundle rb){
		//?nothing?
	}
	
	@FXML
	private void Login(ActionEvent action) throws IOException{
		if("".equals(loginText.getText())){
			//do nothing if field is empty
		}else if(loginText.getText().trim().toLowerCase().equals("admin")){
			
			try{
				Stage stage = new Stage();
				stage.setTitle("Admin Interface");
				AnchorPane myPane = (AnchorPane)FXMLLoader.load(getClass().getClassLoader().getResource("view/Admin.fxml"));
				Scene scene = new Scene(myPane);
				stage.setScene(scene);
				stage.show();
				
			    Node  source = (Node)  action.getSource(); 
			    Stage stage2  = (Stage) source.getScene().getWindow();
			    stage2.close();
				
			} catch(Exception e) {
				e.printStackTrace();
			}
			
		}else{
			for(User user : User.users){
				if(loginText.getText().trim().toLowerCase().equals(user.name)){
					
					for(Album a: user.albums){
						a.albumInit();
						for(Photo p: a.photos){
							p.photoInit();
							for(Tag t: p.tags){
								t.tagInit();
							}
						}
					}
			        FXMLLoader libraryLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/Library.fxml"));
			        Pane libraryPane = (Pane)libraryLoader.load(); 
			        LibraryController controller = (LibraryController) libraryLoader.getController();
			        controller.setUser(user);
			        
			        Stage stage = new Stage();
			        stage.setTitle("Library");
			        Scene scene = new Scene(libraryPane);

			        //controller.setUser(user);   
			        stage.setScene(scene);
			        stage.show();
			        
			        Node  source = (Node)  action.getSource(); 
				    Stage stage2  = (Stage) source.getScene().getWindow();
				    stage2.close();
				}
			}
		}
	}
	
	
}