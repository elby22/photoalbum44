package control;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;



import java.io.*;

import data.User;
import javafx.fxml.Initializable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.FocusModel;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import java.util.Optional;


import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;

/**
 * Controls the Admin.fxml user interface and provides functionality to all FXML objects
 *
 * @author Alex (aer112)
 */
public class AdminController implements Initializable{
	
	@FXML private Button addUser;
	@FXML private Button deleteUser;
	@FXML private Button logout;
	
	@FXML private TextField userText; 
	@FXML private ListView<User> userList;
	
	ObservableList<User> obsUsers;

	
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		//Add Listeners
		//Listener for change of selection
		userList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<User>(){
			public void changed(ObservableValue<? extends User> ov, 
					User old_val, User new_val) {
				
				if(new_val == null){
					userText.setText("");
				}else{
					userText.setText(new_val.name.trim());
				}
			}
		}); 
		
		obsUsers = FXCollections.observableArrayList(User.users);
		userList.setItems(obsUsers);
		userList.getSelectionModel().getSelectedItem();
	}


	@FXML private void addUser(ActionEvent action){
		String temp = userText.getText().trim().toLowerCase();
		if(("").equals(temp)){
			//nothing happens
			return;
		}
		if(!User.users.isEmpty()){
			for(User u : User.users){
				if(u.name.equals(temp)){
					//duplicates not allowed
					System.out.println("Duplicate");
					return;
				}
			}
		}
		
		User user = new User(temp);
		User.users.add(user);
		obsUsers.add(user);
		userList.setItems(obsUsers);
		userList.getSelectionModel().selectFirst();
		User.serialize();
		return;
		
	}
	
	@FXML private void deleteUser(ActionEvent action){
		String temp = userText.getText().trim().toLowerCase();
		if(temp.equals("")){
			//nothing happens
			return;
		}
		for(int i = 0; i < User.users.size(); i++){
			if(User.users.get(i).name.equals(temp)){
				//User found! Removing
				User.users.remove(User.users.get(i));
				obsUsers.remove(obsUsers.get(i));
				userList.setItems(obsUsers);
				userList.getSelectionModel().selectFirst();
				User.serialize();
				return;
			}
		}
		//User does not exist 
		System.out.println("No such user");
		return;
	}	
	
	@FXML private void logout(ActionEvent action){
		
		try{
			Stage stage = new Stage();
			stage.setTitle("Login");
			AnchorPane myPane = (AnchorPane)FXMLLoader.load(getClass().getClassLoader().getResource("view/Login.fxml"));
			Scene scene = new Scene(myPane);
			stage.setScene(scene);
			stage.show();
			
		    Node  source = (Node)  action.getSource(); 
		    Stage stage2  = (Stage) source.getScene().getWindow();
		    stage2.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
